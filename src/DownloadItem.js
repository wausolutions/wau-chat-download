import React, { Component } from 'react';
import './App.css';
import { Button, Radio, Icon } from 'antd';

class DownloadItem extends Component {
    render() {
        const { name, description, version32Link, defaultVersionLink, icon, version: appVersion } = this.props;
        return (
            <div className="download-item" >
                <div class="heading" >
                    <img src={icon} alt="iOS App" />
                    <div > < span className='name' > {name} </span></div >
                </div> <div className="version" ><div ><span>{description} </span></div > {appVersion} </div>
                {/* <a href="http://about.mattermost.com/mattermost-ios-app/" target="_blank">
                                        <img src="https://mattermost.com/wp-content/themes/mattermostv3/img/app-store.png" alt="iOS App" />
                                    </a> */}
                <Button type="primary"
                    icon="download"
                    size={'large'} >
                    <a href={defaultVersionLink}
                        target="_blank"
                        className="btn download-btn" >
                        Download {version32Link ? '64-Bit' : ''} </a>
                </Button>
                <br />
                {version32Link ? <a href={version32Link}> Download 32 - bit </a> : null}
            </div>
        );
    }
}

export default DownloadItem;