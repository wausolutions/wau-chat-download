import React, { Component } from 'react';
import logo from './assets/images/logo.png';
import './App.css';
import "antd/dist/antd.css";
import DownloadItem from './DownloadItem';
import { Layout } from 'antd'

class App extends Component {
  render() {
    const items = [
      {
        name: 'Android',
        description: 'Runs on Android 8.0+',
        appVersion: 'v1.20',
        defaultVersionLink: '/download/WAU_Chat.apk',
        icon: 'https://mattermost.com/wp-content/themes/mattermostv3/img/android-icon.png',
      },
      {
        name: 'Windows',
        description: 'Runs on Windows 7+',
        appVersion: 'v4.2.1',
        defaultVersionLink: '/download/wauchat-setup-4.2.1.exe',
        version32Link: '/download/wauchat-setup-4.2.1-ia32.exe',
        icon: 'https://mattermost.com/wp-content/themes/mattermostv3/img/windows-icon.png',
      },
    ];

    return (
      <Layout>
        <div className="App" >
          <div className="App-header">
            <img src={logo} className="logo" alt="logo" />
            <h2 className='title'>WAU CHAT</h2>
          </div>
          <div className='App-content'>
            <div className='downloads-content'>
              <div><h2>WAU Chat Mobile & Desktop Apps</h2></div>
              <p className="App-intro">
                <ul className='platforms-block'>
                  {items.map((i) => {
                    return <li><DownloadItem {...i} /></li>
                  })}
                </ul>
              </p>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default App;
